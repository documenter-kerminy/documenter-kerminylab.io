# Notes sur la documentation de Kerminy

Imaginons un objet (site, livre, carte...) qui fasse la synthèse de l'expérience Kerminy à un temps T ( que ce temps soit celui de l'impression ou de la consultation).

## Les objectifs de ce projet

1. Trouver les moyens de documenter les actions d'une communauté — à la fois humains et techniques
2. Montrer comment la documentation peut définir la communauté comme territoire (dans quel but?). comment elle peut faire une cartographie de la communauté.
3. Montrer comment la documentation des actions d'une communauté permet de l'inscrire dans le territoire local (pourquoi que local?) - ou plus simplement lui donne une certaine visibilité

Projet de recherche.

Cette expérience est menée par Pyc, Une maison d'édition, Kerminy (Qui ou quelle association?) et Un lieu subjectif.

## Outils

- ~~11ty — pour le rendu du site~~
- Gitlab pour la gestion des versions et la contribution

## Resources

- https://julie-blanc.fr/blog/2020-11-05_chiragan/